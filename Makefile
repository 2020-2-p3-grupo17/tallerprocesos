CC=gcc -Wall -c 
FS=-fsanitize=address,undefined
bin/procesos: obj/main.o
	gcc obj/main.o -o bin/procesos $(FS)

obj/main.o: src/main.c
	mkdir -p obj/
	$(CC) src/main.c -o obj/main.o $(FS) 

.PHONY: clean
clean:
	rm bin/procesos obj/*
