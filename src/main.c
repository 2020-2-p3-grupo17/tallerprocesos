#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <stdlib.h>


int main(int argc, char **argv){
	pid_t pid_hijo;
	for(int i = 1; i < argc; i++){
		char nombre[100];
		memset(nombre,0,100);
		snprintf(nombre, 100, "%d.png", i);
		
		pid_hijo = fork();
		
		if(pid_hijo == 0){
			execl("bin/procesador_png", "bin/procesador_png", argv[i], nombre, NULL);
		}
		
	}
	
	for (int i = 1; i<argc; i++){
		char nombre[100];
		memset(nombre,0,100);
		snprintf(nombre, 100, "%d.png", i);
		int status;
		waitpid(pid_hijo, &status, 0);
		printf("%s (archivo BW %s): status %d\n", argv[i], nombre, WEXITSTATUS(status));
	}
}
